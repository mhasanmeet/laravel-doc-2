# Laravel Installation

1. Windows Installtion
2. Mac Installtion
3. Linux Installtion

## Installation Requirement

#### Server Requirements
  * **Extensions**
    * PDO - PDO is one of the **php extension**n use for communicating database with php programming language. 
    * JSON - In php JSON also use for communicating with data format like, read or write JSON data.
    * BCMath - A **php extension** use for Math realted usages. 
    * CTyep - A **php extension** use for character type cheking like check Alphanumeic character check, number character check, whitespace character check etc.

## Installation

### **By Composer**   
  * **Composer**: Laravel utilize **composer** to it's dependencies. Also anything Install in php by a dependency manager, composer is use. Before going to install, first check if there composer is inatalled.
  * Check if composer is installed or not in cmd prompt: `composer -v`.
  * If your composer is old version update by type in terminal cmd `composer self-update --preview`
  * If composer is installed. then run **create laravel project** commnad `composer create-project {prefer-dist} {project name}`

### **By Laragon**
  * Go ⟶ Menu ⟶ Quick App ⟶ Laravel ⟶ Give name ⟶ Project created

## Browse Project
 * Check php artisan command by typing in cmd ⟶ `php artisan`
 * View Installed Laravel project via php own virtual server by type command ⟶ `php artisan serve`
  > This command will give a virtual link to check laravel project in browser.


  


  




