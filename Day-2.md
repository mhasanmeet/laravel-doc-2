## Configure **.env** file
  * .env is a environment file in Laravel project use for set up project environment. Laravel always given a .env file as example name as **.env example**.
  * For better undersatnd copy that example **.env** file and paste as **.env** file for project configuration usage.

## **Composer Update**
  * Use `Composer update` commnad for update project dependencies / clong from remote respositoty

## **git ignore**
  * **git igonre** is a file that restrict the uninmportant file to upload in remote repository.

## Error Handling
  * For error handling clean browser prvious config and remove cacche use this cmd: `php artisan config:cache`
  * Also use `php artisan cache clear`
  * Also use `php artisan view:cache`
  * Delete or remove files under **bootstrap** folder from project

 