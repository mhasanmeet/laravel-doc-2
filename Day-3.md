## Routes and Basic Static page
  > What is routing and pagination: {brief Description}
  * For web Application we use web routes
  * Check out current route of this project. Goto Routes ⟶ Web.php. You must find this code.
  <!-- ![Routes]() -->
  <img src="Img/routes.png" alt="Routes" width="600"/>
  
  * **use** ⟶ This term is use for when we need any class
  * **Illuminate** is a laravel top level library
  * ```
    Route::get('/', function (){
    return view('welcome');
    }); 
    ```
    ⟶ This is the code in where we whatever put anything will be visible in the browser.
  * So in here **welcome** is a page which will be located in projects ⟶ resources ⟶ **welcome.blade.php**
  * If anything change in **welcome** page will be chnage in the web **welcome** page


## Tasks
  
  - [ ] Create 5 pages, **Home**, **About me**, **Resume**, **Contact**, **My Portfolio**
  - [ ] In the task do not use any blade templating
  - [ ] Use **Plain** Bootstrap and CSS
  - [ ] Learn how to navigate between two page

